#!/usr/bin/env python
# coding=utf-8
import math
import random


class Planet():
    def __init__(self, mass, pos):
        self.mass = mass
        self.x = pos[0]
        self.y = pos[1]
        self.vel = [0.0, 0.0]
        self.acel = [0.0, 0.0]
        self.collided = False
        self.collidedPlanet = None
        self.timesCollided = 0
        self.timesRecollided = 0
        self.radius = 8 if mass <= 0 else math.floor(4 * math.sqrt(mass))
        self.moving = False

    def distance_to(self, other_planet):
        return math.sqrt(math.pow(self.x - other_planet.x, 2) +
                         math.pow(self.y - other_planet.y, 2))

    def set_on_top(self, p, arg):
        modulus = self.radius + p.radius + 2
        pos_x = math.ceil(modulus * math.cos(arg))
        pos_y = math.ceil(modulus * math.sin(math.pi + arg))

        self.x = p.x + pos_x
        self.y = p.y + pos_y

        self.timesRecollided = 15
        self.moving = False
        self.collidedPlanet = p
        # self.adjust()

    def adjust(self):
        d = self.distance_to(
            self.collidedPlanet) - self.radius - self.collidedPlanet.radius
        if d < 0.01:
            module = math.sqrt(math.pow(self.x - self.collidedPlanet.x, 2) +
                               math.pow(self.y - self.collidedPlanet.y, 2))
            correct_distance = self.radius + self.collidedPlanet.radius
            relative_error = abs(d) / self.collidedPlanet.radius
            relative_error = max(relative_error, 0.01)
            scalar = (1 + relative_error) * correct_distance / module
            self.x += scalar * self.x - self.collidedPlanet.x
            self.y += scalar * self.y - self.collidedPlanet.y

    def update(self, delta, planets):
        for planet in planets:
            if not self.collided and (self.distance_to(
                    planet) - self.radius - planet.radius) <= 0.1:
                self.collided = True
                if self.collidedPlanet != planet:
                    self.timesCollided += 1
                    self.timesRecollided = 1
                else:
                    self.timesRecollided += 1
                self.collidedPlanet = planet

            cube_dist = math.pow(self.distance_to(planet), 3)
            self.acel[0] += 10 * (planet.x - self.x) * planet.mass / cube_dist
            self.acel[1] += 10 * (planet.y - self.y) * planet.mass / cube_dist

        # Update velocity
        self.vel[0] += self.acel[0]
        self.vel[1] += self.acel[1]

        if self.collided:
            lap = [0.0, 0.0]
            portog = [0.0, 0.0]
            ux = [0.0, 0.0]
            uy = [0.0, 0.0]
            modulus = self.collidedPlanet.distance_to(self)

            lap[0] = self.collidedPlanet.x - self.x
            lap[1] = self.collidedPlanet.y - self.y
            portog[0] = -lap[1]
            portog[1] = lap[0]
            proyx = (self.vel[0] * lap[0] + self.vel[1] * lap[1]) / modulus
            proyy = (self.vel[0] * portog[0] + self.vel[1] * portog[1]) / modulus

            ux[0] = proyx * lap[0] / modulus
            ux[1] = proyx * lap[1] / modulus

            uy[0] = proyy * portog[0] / modulus
            uy[1] = proyy * portog[1] / modulus

            self.vel = [(-ux[0] - uy[0]) / 2, (-ux[1] - uy[1]) / 2]

        self.x += 2.5 * self.vel[0] / delta
        self.y += 2.5 * self.vel[1] / delta

        self.acel = [0.0, 0.0]

    def copy(self):
        return Planet(self.mass, [self.x, self.y])


def xfrange(start, stop, step):
    old_start = start  # backup this value

    digits = int(round(math.log(10000, 10))) + 1  # get number of digits
    magnitude = 10 ** digits
    stop = int(magnitude * stop)  # convert from
    step = int(magnitude * step)  # 0.1 to 10 (e.g.)

    if start == 0:
        start = 10 ** (digits - 1)
    else:
        start *= 10 ** (digits)

    data = []  # create array

    # calc number of iterations
    end_loop = int((stop - start) // step)
    if old_start == 0:
        end_loop += 1

    acc = start

    for i in xrange(0, end_loop):
        data.append(acc / magnitude)
        acc += step

    return data


class GameState(object):
    def __init__(self, ind):
        self.particle = Planet(ind['particle']['mass'], ind['particle']['pos'])
        self.hole = Planet(ind['hole']['mass'], ind['hole']['pos'])
        self.planets = [Planet(p['mass'], p['pos']) for p in ind['planets']]
        self.particle.set_on_top(self.planets[ind['particle']['over']],
                                 ind['particle']['angle'])
        self.hole.set_on_top(self.planets[ind['hole']['over']],
                             ind['hole']['angle'])

    def simula(self, n):
        alpha = math.atan2(self.hole.y - self.particle.y,
                           self.hole.x - self.particle.x)
        angles = xfrange(alpha - math.pi / 2, alpha + math.pi / 2, math.pi / n)
        return min([self.hit_or_miss(a) for a in angles]) * 1000

    def hit_or_miss(self, angle):
        p = self.particle.copy()
        p.vel = [random.randrange(50.0, 150.0) * math.cos(angle),
                 random.randrange(50.0, 150.0) * math.sin(angle)]
        for _ in range(300):
            p.update(1000.0 / 60.0, self.planets)
        return p.distance_to(self.hole)


if __name__ == '__main__':
    ind = {"planets": [{"pos": [602, 113], "mass": 461.0},
                       {"pos": [312, 289], "mass": 382.0},
                       {"pos": [111, 471], "mass": 392.0},
                       {"pos": [617, 362], "mass": 179.0}],
           "hole": {"over": 0,
                    "pos": [0, 0],
                    "angle": 2.0395028468176126,
                    "mass": 0},
           "particle": {"over": 1,
                        "pos": [0, 0],
                        "angle": 1.454838727202534,
                        "mass": 0}}

    state = GameState(ind)
    print state.simula(100)
