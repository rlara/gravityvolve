#!/usr/bin/env python
# coding=utf-8
import sys
import cPickle as pickle
import pygame
import math

pygame.init()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'USAGE: draw mad_file map_index'
    else:
        with open(sys.argv[1], 'rb') as input_file:
            data = pickle.load(input_file)
        mapa = data[int(sys.argv[2])][0][0]
        window = pygame.display.set_mode((900, 600))

        for p in mapa['planets']:
            pygame.draw.circle(window,
                               (255, 0, 0),
                               (p['pos'][0], p['pos'][1]),
                               4 * int(math.sqrt(p['mass'])))

        pygame.display.flip()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit(0)
