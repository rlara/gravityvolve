__author__ = 'raul'

import unittest
import random
import generator
import evolution


class RandomGenerator(unittest.TestCase):
    def setUp(self):
        self.planet_number = random.randint(4, 6)
        self.level = generator.generate_level(self.planet_number)

    def test_level_structure(self):
        self.assertIsInstance(self.level, dict)
        self.assertIn('planets', self.level)
        self.assertIn('particle', self.level)
        self.assertIn('hole', self.level)
        self.assertIsInstance(self.level['planets'], list)
        self.assertIsInstance(self.level['particle'], dict)
        self.assertIsInstance(self.level['hole'], dict)

    def test_number_of_planets(self):
        self.assertEqual(self.planet_number, len(self.level['planets']))

    def test_planets_structure(self):
        self.assertTrue(all(['pos' in p and 'mass' in p for p in self.level['planets']]))
        self.assertTrue(all([isinstance(p['pos'], list) for p in self.level['planets']]))

    def test_particle_structure(self):
        self.assertIn('angle', self.level['particle'])
        self.assertIn('mass', self.level['particle'])
        self.assertIn('over', self.level['particle'])
        self.assertIn('pos', self.level['particle'])

    def test_hole_structure(self):
        self.assertIn('angle', self.level['hole'])
        self.assertIn('mass', self.level['hole'])
        self.assertIn('over', self.level['hole'])
        self.assertIn('pos', self.level['hole'])

    def tearDown(self):
        pass


class EvolutionaryAlgorithm(unittest.TestCase):

    def setUp(self):
        self.evoparams = {'n': 100,
                          'max_iter': 2000,
                          'n_planets': 6,
                          'mutation_rate': 0.2,
                          'crossover_rate': 0.9,
                          'pop_levels': 3}

    def test_structured_insert(self):
        n = self.evoparams['n']
        pop = [(0, n - x - 1) for x in range(n)]
        evolution.structured_population_insert(pop, (1, 120), 3)
        self.assertIsInstance(pop, list,
                              'The insertion has destroyed the population')
        # (1, 120) should be at position 32
        self.assertEqual(pop[32], (1, 120),
                         'Element has not been inserted in level')

        evolution.structured_population_insert(pop, (2, 37), 3)
        # (2, 37) should be at position 65
        self.assertEqual(pop[65], (2, 37),
                         'Element has not been inserted in level')

        evolution.structured_population_insert(pop, (3, 1), 3)
        # (3, 1) should be at position 99
        self.assertEqual(pop[99], (3, 1),
                         'Element has not been inserted in level')

        evolution.structured_population_insert(pop, (4, -10), 3)
        # (4, -10) should not be in the population
        self.assertNotIn((4, -10), pop, 'Element has been inserted incorrectly')



if __name__ == '__main__':
    unittest.main()
