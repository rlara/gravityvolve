/**
 * Created by raul on 9/09/14.
 */
var loadProgressLabel, loadingBarContainer, loadingBarHeight, loadingBarWidth,
    LoadingBarColor, loadingBar, frame, padding, canvas, stage, preload, backgroundImage,
    planetImage, ballImage, targetImage, background, line, txtMoves, mapFile, txtYouWin, grayrect;
var state, dummy, oracle, last_ball_x, last_ball_y, mapKey, mapData;


function winSplash() {
    grayrect = new createjs.Shape();
    grayrect.graphics.f(createjs.Graphics.getRGB(0, 0, 0, 0.6)).dr(0, 0, canvas.width, canvas.height).ef();
    stage.addChild(grayrect);

    txtYouWin = new createjs.Text("You win!", "72px Audiowide", "#FF7700");
    txtYouWin.lineWidth = 400;
    txtYouWin.textAlign = "center";
    txtYouWin.x = canvas.width/2;
    txtYouWin.y = canvas.height/2 - 80;
    stage.addChild(txtYouWin);

//    txtInfo = new createjs.Text("Please rate this level's difficulty:", "32px Audiowide", "#FF7700");
//    txtInfo.lineWidth = canvas.width;
//    txtInfo.textAlign = "center";
//    txtInfo.x = canvas.width/2;
//    txtInfo.y = canvas.height/2;
//    stage.addChild(txtInfo);
//
//    txtHard = new createjs.Text("HARD", "24px Audiowide", "#FF7700");
//    txtHard.lineWidth = canvas.width/3;
//    txtHard.textAlign = "center";
//    txtHard.x = canvas.width/4;
//    txtHard.y = canvas.height/2+80;
//    var hit = new createjs.Shape();
//	hit.graphics.beginFill("#000").drawRect(
//            -txtHard.getMeasuredWidth()/2 - 20,
//            -txtHard.getMeasuredHeight()/2 - 10,
//            txtHard.getMeasuredWidth() + 40,
//            txtHard.getMeasuredHeight() + 40);
//	txtHard.hitArea = hit;
//    txtHard.addEventListener("click", function(event) {
//        $.post("vote",
//            {
//                "id":mapKey,
//                "value":1
//            },
//            init);
//        //init();
//    });
//    stage.addChild(txtHard);
//
//    txtMed = new createjs.Text("MEDIUM", "24px Audiowide", "#FF7700");
//    txtMed.lineWidth = canvas.width/3;
//    txtMed.textAlign = "center";
//    txtMed.x = canvas.width/2;
//    txtMed.y = canvas.height/2+80;
//    var hit2 = new createjs.Shape();
//	hit2.graphics.beginFill("#000").drawRect(
//            -txtMed.getMeasuredWidth()/2 - 20,
//            -txtMed.getMeasuredHeight()/2 - 10,
//            txtMed.getMeasuredWidth() + 40,
//            txtMed.getMeasuredHeight() + 40);
//	txtMed.hitArea = hit2;
//    txtMed.addEventListener("click", function(event) {
//        $.post("vote",
//            {
//                "id":mapKey,
//                "value":0
//            },
//            init);
////        init();
//    });
//    stage.addChild(txtMed);
//
//    txtEasy = new createjs.Text("EASY", "24px Audiowide", "#FF7700");
//    txtEasy.lineWidth = canvas.width/3;
//    txtEasy.textAlign = "center";
//    txtEasy.x = 3*canvas.width/4;
//    txtEasy.y = canvas.height/2+80;
//    var hit3 = new createjs.Shape();
//	hit3.graphics.beginFill("#000").drawRect(
//            -txtEasy.getMeasuredWidth()/2 - 20,
//            -txtEasy.getMeasuredHeight()/2 - 10,
//            txtEasy.getMeasuredWidth() + 40,
//            txtEasy.getMeasuredHeight() + 40);
//	txtEasy.hitArea = hit3;
//    txtEasy.addEventListener("click", function(event) {
//        $.post("vote",
//            {
//                "id":mapKey,
//                "value":-1
//            },
//            init);
////        init();
//    });
//    stage.addChild(txtEasy);
}


function progressBar() {
    // Preload progress bar
    loadProgressLabel = new createjs.Text("","18px Verdana","black");
    loadProgressLabel.lineWidth = 300;
    loadProgressLabel.textAlign = "center";
    loadProgressLabel.x = canvas.width/2;
    loadProgressLabel.y = 50;
    stage.addChild(loadProgressLabel);

    loadingBarContainer = new createjs.Container();
    loadingBarHeight = 30;
    loadingBarWidth = 500;
    LoadingBarColor = createjs.Graphics.getRGB(0,0,0);
    loadingBar = new createjs.Shape();
    loadingBar.graphics.beginFill(LoadingBarColor).drawRect(0, 0, 1, loadingBarHeight).endFill();

    frame = new createjs.Shape();
    padding = 3;
    frame.graphics.setStrokeStyle(1).beginStroke(LoadingBarColor).drawRect(
        -padding/2,
        -padding/2,
        loadingBarWidth+padding,
        loadingBarHeight+padding
    );
    loadingBarContainer.addChild(loadingBar, frame);
    loadingBarContainer.x = Math.round(canvas.width/2 - loadingBarWidth/2);
    loadingBarContainer.y = 100;
    stage.addChild(loadingBarContainer);
}

function init() {
    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);

    progressBar();

    preload = new createjs.LoadQueue(false);
    preload.addEventListener("complete", handleComplete);
    preload.addEventListener("progress", handleProgress);

    preload.loadManifest([{id: "imgBackground", src: "static/img/space2.jpg"},
        {id: "imgPlanet", src: "static/img/planet.png"},
        {id: "imgBola", src: "static/img/bolaroja2.gif"},
        {id: "imgTarget", src: "static/img/target.png"},
        {id: "mapDesc", src: "map", type: createjs.LoadQueue.JSON}]);
}

function handleProgress() {
    loadingBar.scaleX = preload.progress * loadingBarWidth;
    var progressPercentage = Math.round(preload.progress*100);
    loadProgressLabel.text = progressPercentage + "% Loaded";

    stage.update();
}

function handleComplete() {
    backgroundImage = preload.getResult("imgBackground");
    planetImage = preload.getResult("imgPlanet");
    ballImage = preload.getResult("imgBola");
    targetImage = preload.getResult("imgTarget");
    mapFile = preload.getResult("mapDesc");
    mapKey = mapFile.key;
    if (mapKey == "-1") {
        loadProgressLabel.text = "HOORAY! You have played and voted all maps"
        stage.update();
    } else {

        mapData = mapFile['map-data'];

        stage.removeChild(loadProgressLabel);
        stage.removeChild(loadingBarContainer);

        newGame();
    }
}

function newGame() {
    stage.mouseMoveOutside = true;

    state = {
        moves: 0,
        planets: [],
        hole: new Planet(targetImage, mapData.hole.mass, mapData.hole.pos),
        particle: new Planet(ballImage, mapData.particle.mass, mapData.particle.pos)
    };
    background = new createjs.Bitmap(backgroundImage);
    stage.addChild(background);

    // Load planets descripcion from JSON file

    for (var i in mapData.planets) {
        var p = new Planet(planetImage, mapData.planets[i].mass, mapData.planets[i].pos);
        state.planets.push(p);
        stage.addChild(p);
    }

    // Target
    state.hole.setOnTop(state.planets[mapData.hole.over], mapData.hole.angle);
    stage.addChild(state.hole);

    // Particle
    state.particle.setOnTop(state.planets[mapData.particle.over], mapData.particle.angle);
    state.particle.on("pressmove", dragMove);
    state.particle.on("pressup", dragUp);
    stage.addChild(state.particle);

    txtMoves = new createjs.Text();
    txtMoves.x = 10;
    txtMoves.y = canvas.height - 40;
    txtMoves.font = "30px Audiowide";
    txtMoves.color = "#FF7700";
    txtMoves.text = "Moves: " + state.moves;
    stage.addChild(txtMoves);

    // Aim line
    line = new createjs.Shape();
    line.graphics.setStrokeStyle(1);
    stage.addChild(line);

    oracle = new createjs.Shape();
    stage.addChild(oracle);

    createjs.Ticker.setFPS(60);

    if (!createjs.Ticker.hasEventListener("tick")) {
        createjs.Ticker.on("tick", tick, null, false);
    }
}

function tick(e) {
    if (!e.paused) {
        var delta = 1000/createjs.Ticker.getFPS();
        state.particle.update(delta, state.planets);

        // Checks end game
        if (state.particle.distanceTo(state.hole) <= 0.98 * state.particle.r) {
            endGame();
        }

        if (state.particle.timesRecollided >= 15) {
//            state.particle.adjust();
            state.particle.moving = false;
            createjs.Ticker.setPaused(true);
        }

        // Checks if particle is out of bounds
        var x = state.particle.x;
        var y = state.particle.y;
        if ((x < -canvas.width/6) || (x > 7*canvas.width/6) || (y < -canvas.height/6) || (y > 7*canvas.height/6)) {
            state.particle.x = last_ball_x;
            state.particle.y = last_ball_y;
            state.particle.moving = false;
            createjs.Ticker.setPaused(true);
        }
    }
    stage.update();
}

function dragMove(evt) {
    if (!state.particle.moving) {
        line.graphics.c().ss(3).s("#0F0").mt(state.particle.x, state.particle.y).lt(evt.rawX, evt.rawY).es();

        var pxs = simulation(40, evt.rawX, evt.rawY);
        oracle.graphics.c().ss(1).s("#F0F").mt(state.particle.x, state.particle.y);

        for (var i = 0; i < pxs[0].length; i++) {
            oracle.graphics.lt(pxs[0][i], pxs[1][i]);
            oracle.graphics.mt(pxs[0][i], pxs[1][i]);
        }

        oracle.graphics.es();
    }
}

function dragUp(evt) {
    if (!state.particle.moving) {
        // Deletes the aim and oracle lines from the screen
        line.graphics.c();
        oracle.graphics.c();

        // We need to store the last static position of the ball in order to
        // put it there if the ball goes offbounds
        last_ball_x = state.particle.x;
        last_ball_y = state.particle.y;

        var maxVel = 30;
        var newVelocity = [
            evt.rawX - state.particle.x,
            evt.rawY - state.particle.y
        ];
        var modulo = Math.sqrt(Math.pow(newVelocity[0],2) + Math.pow(newVelocity[1],2));
        if (modulo>=150){
            newVelocity[0]= maxVel * newVelocity[0]/modulo;
            newVelocity[1]= maxVel * newVelocity[1]/modulo;
        } else {
            newVelocity[0] = maxVel/150 * newVelocity[0];
            newVelocity[1] = maxVel/150 * newVelocity[1];
        }

        state.particle.vel[0] = newVelocity[0];
        state.particle.vel[1] = newVelocity[1];
        state.particle.collided = false;
        state.particle.timesRecollided = 0;

        state.particle.moving = true;

        state.moves++;
        txtMoves.text = "Moves: " + state.moves;

        createjs.Ticker.setPaused(false);
    }
}

function endGame() {
    winSplash();
    createjs.Ticker.setPaused(true);
    canvas.onclick = handleClick;
}

function handleClick() {
    canvas.onclick = null;

    state.particle.off("pressmove", dragMove);
    state.particle.off("pressup", dragUp);

    stage.removeAllChildren();

    location.href = "/maps/" + mapKey + "/vote";

//    progressBar();
//
//    preload.loadManifest([{id: "mapDesc", src: "map", type: createjs.LoadQueue.JSON}]);
}

function simulation(nPoints, x, y) {
    dummy = new Planet(planetImage, state.particle.mass, [state.particle.x, state.particle.y]);
    var xs = [];
    var ys = [];

    var maxVel = 30;
    var newVelocity = [
        x - dummy.x,
        y - dummy.y
    ];
    var modulo = Math.sqrt(Math.pow(newVelocity[0],2) + Math.pow(newVelocity[1],2));
    if (modulo>=150){
        newVelocity[0]= maxVel * newVelocity[0]/modulo;
        newVelocity[1]= maxVel * newVelocity[1]/modulo;
    } else {
        newVelocity[0] = maxVel/150 * newVelocity[0];
        newVelocity[1] = maxVel/150 * newVelocity[1];
    }

    dummy.vel[0] = newVelocity[0];
    dummy.vel[1] = newVelocity[1];

    var delta = 1000/createjs.Ticker.getFPS();

    for (var i = 0; i < nPoints; i++) {
        dummy.update(delta, state.planets);
        xs.push(dummy.x);
        ys.push(dummy.y);
    }

    return [xs, ys];
}
