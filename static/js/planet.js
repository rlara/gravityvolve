/**
 * Created by raul on 9/09/14.
 */
function modulo(pos1, pos2) {
    if (pos1.length === pos2.length){
        var parcial, total = 0;
        for (var i=0;i<pos1.length;i++){
            parcial = Math.pow(pos1[i] - pos2[i], 2);
            total += parcial;
        }
        return Math.sqrt(total);
    }else{
        return 0.0;
    }
}

function distancia(planet1, planet2) {
    return planet1.distanceTo(planet2) - planet1.r - planet2.r;
}


function Planet(image, masa, pos) {
    this.initialize(image, masa, pos);
}

Planet.prototype = new createjs.Bitmap();
Planet.prototype.Bitmap_initialize = Planet.prototype.initialize;
Planet.prototype.initialize = function(image, masa, pos) {
    this.Bitmap_initialize(image);
    this.snapToPixel = false;
    this.masa = masa;
    this.x = pos[0];
    this.y = pos[1];
    this.vel = [0, 0];
    this.acel = [0, 0];
    this.collided = false;
    this.collidedPlanet = null;
    this.timesCollided = 0;
    this.timesRecollided = 0;
    this.regX = this.image.width / 2;
    this.regY = this.image.height / 2;
    this.r = masa>0 ? 4 * Math.sqrt(masa) : 4 * Math.sqrt(4);
    this.scaleX = this.r/this.regX;
    this.scaleY = this.r/this.regY;
    this.moving = false;

    // Public methods
    this.distanceTo = function (otherPlanet) {
        return modulo([this.x, this.y], [otherPlanet.x, otherPlanet.y]);
    };

    this.setOnTop = function(otherPlanet, arg) {
        var modulo = this.r + otherPlanet.r + 2;
        var posX = Math.ceil(modulo * Math.cos(arg));
        var posY = Math.ceil(modulo * Math.sin(Math.PI+arg));

        this.x = otherPlanet.x + posX;
        this.y = otherPlanet.y + posY;

        this.timesRecollided = 15;
        this.moving = false;
        this.collidedPlanet = otherPlanet;
//        this.adjust();
    };

    this.adjust = function() {
        var d = distancia(this, this.collidedPlanet);

        if (d <= 0.01) {
            var vector = [
                this.x - this.collidedPlanet.x,
                this.y - this.collidedPlanet.y
            ];
            var module = Math.sqrt(Math.pow(vector[0], 2) + Math.pow(vector[1], 2));
            var correctDistance = this.r + this.collidedPlanet.r;
            var relativeError = Math.abs(d)/this.collidedPlanet.r;
            relativeError = Math.max(relativeError, 0.01);
            var scalar = (1 + relativeError) * correctDistance/module;
            this.x += scalar*vector[0];
            this.y += scalar*vector[1];
        }
    };

    this.update = function(timeDelta, planets) {
        for(var i=0; i < planets.length; i++) {

            if (!this.collided && (distancia(this, planets[i]) <= 0.1)){
                //console.log(distancia(this, planets[i]))
                this.collided = true;

                // Collisions stats
                if (this.collidedPlanet != planets[i]) {
                    this.timesCollided++;
                    this.timesRecollided = 1;
                } else {
                    this.timesRecollided++;
                }

                this.collidedPlanet = planets[i];

            } else {
                if (this.collidedPlanet == planets[i]) {
                    this.collided = false;
                }
            }
            var cubeDist = Math.pow(this.distanceTo(planets[i]), 3);
            this.acel[0] += 10 * (planets[i].x - this.x) * planets[i].masa / cubeDist;
            this.acel[1] += 10 * (planets[i].y - this.y) * planets[i].masa / cubeDist;
        }

        // Update velocity
        this.vel[0] += this.acel[0]/timeDelta * timeDelta;
        this.vel[1] += this.acel[1]/timeDelta * timeDelta;

        // Update position
        if (this.collided) {
            var P = [0, 0];
            var Portog = [0, 0];
            var Ux = [0, 0];
            var Uy = [0, 0];
            var modulo = this.collidedPlanet.distanceTo(this);
            var proyX, proyY;

            P[0] = this.collidedPlanet.x - this.x;
            P[1] = this.collidedPlanet.y - this.y;
            Portog[0] = -P[1];
            Portog[1] = P[0];
            proyX = (this.vel[0]*P[0]+this.vel[1]*P[1])/modulo;
            proyY = (this.vel[0]*Portog[0]+this.vel[1]*Portog[1])/modulo;

            Ux[0] = proyX*P[0] / modulo;
            Ux[1] = proyX*P[1] / modulo;

            Uy[0] = proyY * Portog[0] / modulo;
            Uy[1] = proyY * Portog[1] / modulo;

            this.vel[0] = (-Ux[0]+Uy[0])/2;
            this.vel[1] = (-Ux[1]+Uy[1])/2;

            this.x += 2.5 * this.vel[0]/timeDelta;
            this.y += 2.5 * this.vel[1]/timeDelta;
        } else {
            this.x += 2.5 * this.vel[0]/timeDelta;
            this.y += 2.5 * this.vel[1]/timeDelta;
        }

        this.acel = [0, 0];
    }
};
