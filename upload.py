#!/usr/bin/env python
# coding=utf-8

import cPickle as pickle
import sys
import urllib
import urllib2


def load_object(path):
    with open(path, 'r') as input_file:
        return pickle.load(input_file)


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print 'USAGE: upload map_file url fitness_type'
    else:
        filename = sys.argv[1]
        url = sys.argv[2]
        ftype = sys.argv[3]
        res = load_object(filename)

        for item in res:
            params = urllib.urlencode({
                'fitness-type': ftype,
                'fitness': item[0][1],
                'level': item[1],
                'map-data': item[0][0]
            })
            response = urllib2.urlopen(url, params).read()
            print '%f %d saved! Response: %s' % (item[0][1], item[1], response)
