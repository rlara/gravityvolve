#!/usr/bin/env python
# coding=utf-8

# planet = {
# 'mass': 10,
# 'pos': [0, 0]}
from operator import itemgetter
import urllib
from generator import generate_level, CANVAS_HEIGHT, CANVAS_WIDTH
import random
import math
import game
import cPickle as pickle
import urllib2
import generator


def division(m, n, level):
    lower = filter(lambda p: p['pos'][1] <= m * p['pos'][0] + n,
                   level['planets'])
    upper = filter(lambda p: p['pos'][1] > m * p['pos'][0] + n,
                   level['planets'])
    return lower, upper


def fitness_simulation(ind):
    # if not generator.level_test(ind):
    #     return -float("inf")
    state = game.GameState(ind)
    return state.simula(10)


def fitness_intersections(ind):
    # if not generator.level_test(ind):
    #     return -float("inf")

    fitness = 0.0
    # TODO: Demasiados mapas con fitness 0, comprobar
    # STEP 1: Line between particle's planet and hole's planet
    particle = ind['planets'][ind['particle']['over']]
    hole = ind['planets'][ind['hole']['over']]

    # Special Case: x1 == x2
    if hole['pos'][0] - particle['pos'][0] == 0:
        idxs = range(len(ind['planets']))
        idxs.remove(ind['particle']['over'])
        idxs.remove(ind['hole']['over'])
        for i in idxs:
            p = ind['planets'][i]
            r = 4 * math.sqrt(p['mass'])
            if abs(hole['pos'][0] - p['pos'][0]) < r:
                fitness += math.sqrt(
                    r * r - math.pow(abs(hole['pos'][0] - p['pos'][0]), 2))
        return fitness

    m = float(hole['pos'][1] - particle['pos'][1]) / float(
        hole['pos'][0] - particle['pos'][0])
    n = -m * particle['pos'][0] + particle['pos'][1]

    # STEP 2: Check planet intersection with the previously defined line
    idxs = range(len(ind['planets']))
    idxs.remove(ind['particle']['over'])
    idxs.remove(ind['hole']['over'])
    for i in idxs:
        p = ind['planets'][i]
        r = 4 * math.sqrt(p['mass'])
        a = p['pos'][0]
        b = p['pos'][1]
        disc = r * r * m * m + r * r - a * a * m * m + 2 * a * b * m - 2 * a * m * n - b * b + 2 * b * n - n * n
        # STEP 3: Compute modulus
        if disc > 0:
            x0 = math.sqrt(disc) / (m * m + 1) + (a + b * m - m * n) / (
                m * m + 1)
            y0 = (a + b * m - m * n) / (m * m + 1)
            x1 = -math.sqrt(disc) / (m * m + 1) + (a + b * m - m * n) / (
                m * m + 1)
            y1 = y0
            fitness += math.sqrt(math.pow(x1 - x0, 2) + math.pow(y1 - y0, 2))
    return fitness


def accelby(p, pos):
    assert p['pos'] != pos
    assert p['mass'] != 0
    d = math.sqrt(math.pow(pos[0] - p['pos'][0], 2) +
                  math.pow(pos[1] - p['pos'][1], 2))
    d = math.pow(d, 3)
    try:
        return (p['pos'][0] - pos[0]) * p['mass'] / d, \
               (p['pos'][1] - pos[1]) * p['mass'] / d
    except ZeroDivisionError:
        print p['pos'], pos, p['mass']


def set_on(planet, d, arg):
    x = (d * math.cos(arg)) + planet['pos'][0]
    y = (d * math.sin(math.pi + arg)) + planet['pos'][1]
    return x, y,


def fitness_atraction(ind):
    # if not generator.level_test(ind):
    #     return -float("inf")

    mean_accel = 0.0
    target = ind['planets'][ind['hole']['over']]
    args = [i * 2 * math.pi / 50.0 for i in range(50)]
    d = 4 * math.sqrt(ind['hole']['mass']) + 8.0
    for arg in args:
        pos = set_on(target, d, arg)
        b = [target['pos'][0] - pos[0], target['pos'][1] - pos[1]]
        newacel = [0.0, 0.0]
        for p in ind['planets']:
            temp = accelby(p, pos)
            newacel[0] += temp[0]
            newacel[1] += temp[1]
        temp = accelby(target, pos)
        proy = (b[0] * newacel[0] + b[1] * newacel[1]) / d
        proytarget = (b[0] * temp[0] + b[1] * temp[1]) / d
        if proy > proytarget:
            proy = proytarget

        mean_accel += proy
    return -mean_accel / len(args)


def crossover(ind1, ind2, rnd):
    valid = False
    while not valid:
        x1 = rnd.randint(0, CANVAS_WIDTH)
        y1 = rnd.randint(0, CANVAS_HEIGHT)
        x2 = rnd.randint(0, CANVAS_WIDTH)
        while x2 == x1:
            x2 = rnd.randint(0, CANVAS_WIDTH)
        y2 = rnd.randint(0, CANVAS_HEIGHT)

        m = float(y2 - y1) / float(x2 - x1)
        n = -m * x1 + y1

        low1, up1 = division(m, n, ind1)
        low2, up2 = division(m, n, ind2)

        valid = len(low1) == len(low2) and \
            len(up1) == len(up2) and \
            low1 != 0 and \
            up1 != 0

        ind1['planets'] = low1 + up2
        ind2['planets'] = low2 + up1


def mutate(ind, prob, mut_rate, rnd):
    for p in ind['planets']:
        # Position x
        if rnd.random() <= prob:
            p['pos'][0] += int(rnd.gauss(0.0, 50.0))
        # Position y
        if rnd.random() <= prob:
            p['pos'][1] += int(rnd.gauss(0.0, 50.0))
        # Mass
        if rnd.random() <= prob:
            p['mass'] += int(rnd.gauss(0.0, 50.0))
            p['mass'] = min([max([p['mass'], 100]), 900])

    # Particle
    if rnd.random() <= mut_rate:
        ind['particle']['over'] = rnd.randint(0, len(ind['planets']) - 1)
        while ind['hole']['over'] == ind['particle']['over']:
            ind['particle']['over'] = rnd.randint(0, len(ind['planets']) - 1)
    if rnd.random() <= mut_rate:
        ind['particle']['angle'] = rnd.uniform(0, math.pi)
    # Hole
    if rnd.random() <= mut_rate:
        ind['hole']['over'] = rnd.randint(0, len(ind['planets']) - 1)
        while ind['hole']['over'] == ind['particle']['over']:
            ind['hole']['over'] = rnd.randint(0, len(ind['planets']) - 1)
    if rnd.random() <= mut_rate:
        ind['hole']['angle'] = rnd.uniform(0, math.pi)


def structured_population_insert(pop, ind, levs):
    i = 1.0
    while i <= levs and ind[1] <= pop[int(math.floor(i * len(pop) / levs)) - 1][1]:
        i += 1.0
    if i < 4:
        pop[int(math.floor(i * len(pop) / levs)) - 1] = ind


def evolution_step(crossover_rate, fitness, mutation_rate, n, pop, pop_levels,
                   rnd):
    # Steady-state algorithm
    # Individuals, binary tournament selection
    a, b = rnd.randint(0, n - 1), rnd.randint(0, n - 1)
    c, d = rnd.randint(0, n - 1), rnd.randint(0, n - 1)
    idx1 = a if pop[a][1] >= pop[b][1] else b
    idx2 = c if pop[c][1] >= pop[d][1] else d
    # Copy of parents
    breed1 = pop[idx1][0].copy()
    breed2 = pop[idx2][0].copy()
    # Crossover?
    if rnd.random() <= crossover_rate:
        crossover(breed1, breed2, rnd)
    # # Mutation
    mutate(breed1, 1.0 / (3 * n), mutation_rate, rnd)
    mutate(breed2, 1.0 / (3 * n), mutation_rate, rnd)
    # Re-evaluation of changed individuals
    breed1 = (breed1, fitness(breed1))
    breed2 = (breed2, fitness(breed2))
    # Population Replacement (stratified population)
    structured_population_insert(pop, breed1, pop_levels)
    structured_population_insert(pop, breed2, pop_levels)


def genetic_algorithm(fitness, n=100, max_iter=100, n_planets=5,
                      mutation_rate=0.15, crossover_rate=0.9,
                      pop_levels=3, seed=None):
    rnd = random.Random()
    rnd.seed(seed)
    # New random population
    maps = [generate_level(n_planets, rnd) for _ in range(n)]
    evals = [fitness(ind) for ind in maps]

    # population list = [(map..., fitness), ...]
    pop = sorted(zip(maps, evals), key=lambda individual: individual[1],
                 reverse=True)

    # Main loop
    for it in range(max_iter):
        evolution_step(crossover_rate, fitness, mutation_rate, n, pop,
                       pop_levels, rnd)
        pop = filter(lambda ind_fit: generator.level_test(ind_fit[0]), pop)
        new_maps = [generate_level(n_planets, rnd) for _ in range(n-len(pop))]
        new_evals = [fitness(new_ind) for new_ind in new_maps]
        pop += zip(new_maps, new_evals)

        pop = sorted(pop, key=itemgetter(1), reverse=True)
        print 'Iteration %d' % it

    idxs = [0,
            n / pop_levels - 1,
            n / pop_levels,
            n / 2,
            2 * n / pop_levels - 1,
            2 * n / pop_levels,
            n - 1]
    levels_list = [1, 1, 2, 2, 2, 3, 3]
    maps_to_pool = [pop[i] for i in idxs]

    return zip(maps_to_pool, levels_list)


def mean_stdev(seq):
    mean = sum(seq) / len(seq)
    sd = [math.pow(x - mean, 2) for x in seq]
    sd = math.sqrt(sum(sd) / len(sd))
    return mean, sd


def log_stats(output, it, pop):
    fitnesses = map(lambda (_, fitn): fitn, pop)
    best = max(fitnesses)
    mean, sd = mean_stdev(fitnesses)
    print it, best, mean, sd


def sum_mass_fitness(ind):
    return sum([p['mass'] for p in ind['planets']])


if __name__ == '__main__':
    def save_object(obj, filename):
        with open(filename, 'wb') as output:
            pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)
    # TODO: Visual feedback to evolutionary progress
    res = genetic_algorithm(fitness_intersections, max_iter=5000, seed=None)
    save_object(res, 'intersections.dat')

    res = genetic_algorithm(fitness_atraction, max_iter=5000, seed=None)
    save_object(res, 'atractions.dat')

    res = genetic_algorithm(fitness_simulation, max_iter=5000, seed=None)
    save_object(res, 'simulations.dat')
