#!/usr/bin/env python
# coding=utf-8

from google.appengine.ext import ndb


class Experiment(ndb.Model):
    date = ndb.DateTimeProperty(auto_now_add=True)
    fitness = ndb.IntegerProperty(choices=[1, 2, 3])
    seed = ndb.IntegerProperty(required=True)
    pop_size = ndb.IntegerProperty(required=True)
    max_iter = ndb.IntegerProperty(required=True)
    num_planets = ndb.IntegerProperty(required=True)
    mutation_rate = ndb.FloatProperty(required=True)
    crossover_rate = ndb.FloatProperty(required=True)
    pop_levels = ndb.IntegerProperty(required=True)
    progress = ndb.IntegerProperty(default=0)
    status = ndb.ComputedProperty(lambda self: 'Running' if self.progress < 100 else 'Finished')


class Map(ndb.Model):
    ftype = ndb.IntegerProperty(required=True)
    fitness = ndb.FloatProperty(required=True)
    level = ndb.IntegerProperty(required=True)
    map_data = ndb.JsonProperty(required=True)
    ftype_string = ndb.ComputedProperty(
        lambda s: ['Intersections', 'Atraction', 'Simulations'][s.ftype - 1]
    )
    level_string = ndb.ComputedProperty(
        lambda s: ['Hard', 'Medium', 'Easy'][s.level - 1]
    )


class Player(ndb.Model):
    user_id = ndb.IntegerProperty(required=True)

    @classmethod
    def get_user_by_id(cls, user_id):
        return cls.query(cls.user_id == user_id).fetch()


class Vote(ndb.Model):
    map_key = ndb.KeyProperty(kind=Map, required=True, indexed=True)
    user_id = ndb.KeyProperty(kind=Player, required=True, indexed=True)
    value = ndb.IntegerProperty(required=True)

    @classmethod
    def votes_by_user(cls, user):
        return cls.query(user_id=user.key)

    @classmethod
    def votes_by_map(cls, themap):
        qry = cls.query(Vote.map_key == themap.key)
        votes = qry.fetch(projection=[Vote.value])
        return [vote.value for vote in votes]
