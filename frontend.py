#!/usr/bin/env python
# coding=utf-8
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import os
import json
import random
from google.appengine.ext.ndb import Key

import webapp2
import jinja2
from webapp2_extras import sessions, routes

import generator
from data_models import Map, Player, Vote

# TODO: Refactoring general, agrupando en modulos
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(
        os.path.join(os.path.dirname(__file__), 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

config = {'webapp2_extras.sessions': {
    'secret_key': 'B1et9mVradFDVfLfYr1EfrVmJD3aytLJ7lXGHYadGoAvSC1SeCIrPfkt47c5nWO2',
    'cookie_args': {
        'max_age': 7776000,
        'domain': None,
        'path': '/',
        'secure': None,
        'httponly': False,
    },
}}


class BaseSessionHandler(webapp2.RequestHandler):
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)

    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        return self.session_store.get_session()


class MainHandler(BaseSessionHandler):
    def get(self):
        if self.session.get('user_id'):
            user_id = self.session.get('user_id')
            qry = Player.query(Player.user_id == user_id).fetch()
            if not qry:
                user = Player(user_id=user_id)
                user.put()
            else:
                user = qry[0]
        else:
            qry = Player.query().order(-Player.user_id).fetch(1)
            # TODO: Comportamiento extraño cuando no hay ningún usuario en la BDD
            user_id = qry[0].user_id + 1 if len(qry) > 0 else 0
            self.session['user_id'] = user_id
            user = Player(user_id=user_id)
            user.put()

        # player = Player.query(Player.user_id == user_id).fetch()[0]

        template = JINJA_ENVIRONMENT.get_template('game.html')
        template_params = {'canvas_width': generator.CANVAS_WIDTH,
                           'canvas_height': generator.CANVAS_HEIGHT,
                           'menu': {
                               'maps': webapp2.uri_for('map-list')
                           },
                           'total_maps': len(Map.query().fetch()),
                           'maps_done': len(Vote.query(Vote.user_id == user.key).fetch())
        }
        self.response.write(template.render(template_params))


class NextMap(BaseSessionHandler):
    def get(self):
        if not self.session.get('user_id'):
            self.error(403)
        else:
            user_id = self.session.get('user_id')
            if not Player.query(Player.user_id == user_id).fetch():
                player = Player(user_id=user_id)
                player.put()
            else:
                # Get the player instance
                player = Player.query(Player.user_id == user_id).fetch()[0]
            qry_voted_maps = Vote.query(Vote.user_id == player.key).fetch()
            voted_maps = [m.map_key for m in qry_voted_maps]
            qry_candidate_maps = Map.query().fetch(keys_only=True)
            candidate_maps = filter(lambda m: m not in voted_maps,
                                    qry_candidate_maps)
            if not candidate_maps:
                return self.response.write("{\"key\": \"-1\"}")
            the_map = random.choice(candidate_maps).get()
            # Generate random map
            logging.info("Fitness type: %s" % the_map.ftype_string)
            return self.response.write("{\"key\": \"%s\", \"map-data\": %s}" %
                                       (the_map.key.urlsafe(),
                                        the_map.map_data[
                                        1:len(the_map.map_data) - 1].replace(
                                            "\'", "\"")))


class MapListHandler(BaseSessionHandler):
    def get(self):
        query = Map.query().order(Map.ftype, -Map.fitness)

        maps = []
        votes = []
        for m in query:
            maps.append(m)
            thevotes = Vote.votes_by_map(m)
            votes.append(
                (
                    thevotes.count(1),
                    thevotes.count(0),
                    thevotes.count(-1)
                )
            )
        # TODO: Eye-candy, colorear las votaciones que no coinciden con la catalogación del mapa
        template_values = {
            'maps_votes': zip(maps, votes),
            'menu': {
                'maps': webapp2.uri_for('map-list')
            }
        }
        template = JINJA_ENVIRONMENT.get_template('maps.html')
        return self.response.write(template.render(template_values))


class LoadMap(webapp2.RequestHandler):
    def get(self):
        return self.response.write("I'm in!")

    def post(self):
        fitness_type = int(self.request.get('fitness-type'))
        fitness = float(self.request.get('fitness'))
        level = int(self.request.get('level'))
        map_data = self.request.get('map-data')

        mapa = Map(ftype=fitness_type,
                   fitness=fitness,
                   level=level,
                   map_data=json.dumps(map_data))
        mapa.put()
        self.response.write('OK!')


class MapInfoHandler(BaseSessionHandler):
    def get(self, map_key):
        pass


class VoteMap(BaseSessionHandler):
    def get(self, map_key):
        template = JINJA_ENVIRONMENT.get_template('vote.html')
        template_params = {
            'menu': {
                'maps': webapp2.uri_for('map-list')
            }
        }
        return self.response.write(template.render(template_params))

    def post(self, map_key):
        if not self.session.get('user_id'):
            self.error(403)
        else:
            user_id = self.session.get('user_id')
            if not Player.query(Player.user_id == user_id).fetch():
                self.error(403)
            else:
                # Get the player instance
                player = Player.query(Player.user_id == user_id).fetch()[0]
                # current_map = Key('Map', int(self.request.get('id')))
                current_map = Key(urlsafe=map_key)
                vote_value = int(self.request.get('rbDiff'))
                current_player = Key('Player', player.key.id())
                vote = Vote(map_key=current_map,
                            user_id=current_player,
                            value=vote_value)
                vote.put()
                return self.redirect('/')


SITE_URLS = [
    webapp2.Route(r'/', handler=MainHandler, name='home'),
    webapp2.Route(r'/map', handler=NextMap, name='get-map'),
    webapp2.Route(r'/load', handler=LoadMap, name='load-map'),
    routes.PathPrefixRoute('/maps', [
        webapp2.Route('/<map_key>/vote', handler=VoteMap, name='vote-map'),
        webapp2.Route('/<map_key>', handler=MapInfoHandler, name='show-map'),
        webapp2.Route('/', handler=MapListHandler, name='map-list')
    ])
]

app = webapp2.WSGIApplication(SITE_URLS, debug=False, config=config)
