#!/usr/bin/env python
# coding=utf-8
import math
import random

CANVAS_WIDTH = 900
CANVAS_HEIGHT = 600


def distance(p, q):
    euclid = int(math.sqrt(math.pow(p['pos'][0] - q['pos'][0], 2) +
                       math.pow(p['pos'][1] - q['pos'][1], 2)))
    pr = 4 * int(math.sqrt(p['mass']))
    qr = 4 * int(math.sqrt(q['mass']))
    return euclid - pr - qr


def level_test(level):
    def r(p):
        return 4 * int(math.sqrt(p['mass']))

    def x(p):
        return p['pos'][0]

    def y(p):
        return p['pos'][1]

    i = 0
    while i < len(level['planets']):
        j = i + 1
        r1 = 4 * int(math.sqrt(level['planets'][i]['mass']))
        if level['planets'][i]['pos'][0] - r1 - 20 <= 0 or \
           level['planets'][i]['pos'][0] + r1 + 20 >= CANVAS_WIDTH or \
           level['planets'][i]['pos'][1] - r1 - 20 <= 0 or \
           level['planets'][i]['pos'][1] + r1 + 20 >= CANVAS_HEIGHT:
            return False

        while j < len(level['planets']):
            d = distance(level['planets'][i], level['planets'][j])
            if d < 20:
                return False
            j += 1
        i += 1
    assert all([x(p) - r(p) - 20 >= 0 and
                x(p) + r(p) + 20 <= CANVAS_WIDTH and
                y(p) - r(p) - 20 >= 0 and
                y(p) + r(p) + 20 <= CANVAS_HEIGHT for p in level['planets']])
    return True


def generate_level(num_planets, rnd=None):
    if not rnd:
        rnd = random.Random()
    valid = False
    while not valid:
        planets = []
        for _ in range(num_planets):
            planets.append({'mass': int(min([max([math.floor(rnd.gauss(500, 200)),
                                              100]),
                                         900])),
                            'pos': [rnd.randint(0, CANVAS_WIDTH),
                                    rnd.randint(0, CANVAS_HEIGHT)]})
        over = rnd.randint(0, len(planets) - 1)
        pover = over
        while pover == over:
            pover = rnd.randint(0, len(planets) - 1)
        level = {
            'hole': {
                'angle': rnd.uniform(0, math.pi),
                'mass': 0,
                'over': over,
                'pos': [0, 0]
            },
            'particle': {
                'angle': rnd.uniform(0, math.pi),
                'mass': 0,
                'over': pover,
                'pos': [0, 0]
            },
            'planets': planets
        }
        valid = level_test(level)
    return level

if __name__ == '__main__':
    a = generate_level(4)
    print a